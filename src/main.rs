extern crate essocony;

use std::env;

fn main() {
  let port: &str = &format!("0.0.0.0:{}", env::var("PORT").unwrap());
  essocony::main(port);
}
