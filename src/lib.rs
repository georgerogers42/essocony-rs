extern crate glob;
extern crate chrono;
extern crate rustc_serialize;
extern crate url;
extern crate iron;
extern crate router;
extern crate handlebars as hbs;
extern crate staticfile;
extern crate mount;
extern crate hoedown;
extern crate hyper;

use iron::prelude::*;

pub fn main(p : &str) {
  let r = app::routes();
  println!("Listening on {}", p);
  Iron::new(r).http(p).unwrap();
}

pub mod app;
