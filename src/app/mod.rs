use iron::prelude::*;
use router::Router;
use mount::Mount;
use staticfile::Static;
use hbs::Handlebars;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;

pub mod articles;
pub mod handlers;

pub type Routes = Chain;

fn load_template(tpl: &mut Handlebars, name: &str, file: &str) {
  let p = Path::new(file);
  let mut fd = File::open(p).unwrap();
  let mut s = String::new();
  fd.read_to_string(&mut s).unwrap();
  tpl.register_template_string(name, s).unwrap();
}

pub fn routes() -> Routes {
  let mut r = Router::new();
  let mut articles_vec = articles::load_articles_vec();
  articles_vec.sort_by(|a, b| {
    b.meta.date().expect("Invalid date").cmp(&a.meta.date().expect("Invalid Date"))
  });
  let x = {
    let mut tpl = Handlebars::new();
    load_template(&mut tpl, "index", "templates/index.hbs");
    let avec = articles_vec.clone();
    move |req: &mut Request| -> IronResult<Response> {
      handlers::index(&avec, &tpl, req)
    }
  };
  let y = {
    let mut tpl = Handlebars::new();
    load_template(&mut tpl, "article", "templates/article.hbs");
    let avec = articles_vec.clone();
    let articles_map = articles::load_articles_map();
    move |req: &mut Request| -> IronResult<Response> {
      handlers::article(&avec, &articles_map, &tpl, req)
    }
  };
  r.get("/", x);
  r.get("/article/:title", y);
  let mut m = Mount::new();
  m.mount("/", r);
  m.mount("/static/", Static::new("public/static"));
  Chain::new(m)
}
