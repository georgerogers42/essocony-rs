use std::collections::*;
use app::articles;
use iron::prelude::*;
use iron::headers::ContentType;
use router::*;
use hbs::*;
use rustc_serialize::json::*;

fn respond(tpl: &Handlebars, name: &str, data: &Json) -> IronResult<Response> {
    let mut resp = Response::with(tpl.render(name, data).unwrap());
    resp.headers.set(ContentType::html());
    Ok(resp)

}

pub fn index(arts: &Vec<articles::Article>, tpl: &Handlebars, _req: &mut Request) -> IronResult<Response> {
    let mut amap = BTreeMap::new();
    amap.insert("articles".to_string(), arts.to_json());
    respond(tpl, "index", &amap.to_json())
}

pub fn article(arts: &Vec<articles::Article>, amap: &HashMap<String, articles::Article>, tpl: &Handlebars, req: &mut Request) -> IronResult<Response> {
    let title = req.extensions.get::<Router>().unwrap().find("title").unwrap();
    let art = amap.get(title).unwrap();
    let mut x = BTreeMap::new();
    x.insert("article".to_string(), art.to_json());
    x.insert("articles".to_string(), arts.to_json());
    respond(tpl, "article", &x.to_json())
}
