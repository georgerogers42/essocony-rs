use chrono::{DateTime, UTC};
use std::collections::*;
use rustc_serialize::json;
use rustc_serialize::json::*;
use std::fs::File;
use std::io::*;
use hoedown::*;
use hoedown::renderer::html;
use glob::glob;

#[derive(RustcDecodable, RustcEncodable, Clone)]
pub struct Metadata {
  pub title: String,
  pub author: String,
  pub slug: String,
  pub posted: String
}

impl Metadata {
  pub fn date(&self) -> Option<DateTime<UTC>> {
    DateTime::parse_from_rfc3339(&self.posted).ok().map(|x| { x.with_timezone(&UTC) })
  }
}

impl ToJson for Metadata {
  fn to_json(&self) -> Json {
    let mut m = BTreeMap::new();
    m.insert("title".to_string(), self.title.to_json());
    m.insert("author".to_string(), self.author.to_json());
    m.insert("slug".to_string(), self.slug.to_json());
    m.insert("posted".to_string(), self.posted.to_json());
    m.to_json()
  }
}

#[derive(RustcDecodable, RustcEncodable, Clone)]
pub struct Article {
  pub meta: Metadata,
  pub contents: String
}

impl ToJson for Article {
  fn to_json(&self) -> Json {
    let mut m = BTreeMap::new();
    m.insert("meta".to_string(), self.meta.to_json());
    m.insert("contents".to_string(), self.contents.to_json());
    m.to_json()
  }
}

fn read_paragraph<A: BufRead>(fp: &mut A) -> String {
  let mut b = String::new();
  loop {
    let mut l = String::new();
    fp.read_line(&mut l).ok().expect("Read Failure");
    if &l == "\n" {
      return b;
    } else {
      b = b + &l;
    }
  }
}

pub fn load_file(fname : &str) -> Option<Article> {
  let mut fp = BufReader::new(File::open(fname).ok().expect("File opening error"));
  let s = read_paragraph(&mut fp);
  match json::decode::<Metadata>(&s).ok() {
    Some(meta) => {
      let mut c = String::new();
      fp.read_to_string(&mut c).ok().expect("Read Failure");
      let md = Markdown::new(&c);
      let mut html = Html::new(html::Flags::empty(), 0);
      let d = html.render(&md).to_str().unwrap().to_string();
      Some(Article { meta: meta, contents: d })
    }, None => {
      None
    }
  }
}

pub fn load_articles_vec() -> Vec<Article> {
  let mut a = Vec::new();
  for path in glob("articles/*.md").ok().expect("Read directory failure") {
    match load_file(path.unwrap().to_str().unwrap()) {
      Some(art) => {
        a.push(art);
      }, None => {}
    }
  }
  a
}

pub fn load_articles_map() -> HashMap<String, Article> {
  let mut amap = HashMap::new();
  let articles = load_articles_vec();
  for article in articles {
    amap.insert(article.meta.slug.clone(), article);
  }
  amap
}

