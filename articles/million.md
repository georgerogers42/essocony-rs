{"title": "Crackshack or Mansion", "author": "George Rogers", "slug": "crackshack-or-mansion", "posted": "2015-10-01T11:23:00-05:00"}

# Which is the million dollar house?

<table>
<tr><th>1</th><td><a target="top" href="http://www.zillow.com/homedetails/1531-Tyler-Park-Way-Mountain-View-CA-94040/19534340_zpid/"><img src="/static/million.jpg"></td></a></tr>
<tr><th>2</th><td><a target="top" href="http://www.zillow.com/homedetails/4-Wedgewood-Pt-Spring-TX-77381/28772166_zpid/"><img src="/static/not-a-million.jpg"></a></td></tr>
</table>
