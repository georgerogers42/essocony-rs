{"title": "Escape from New York", "author": "George Rogers", "slug": "escape-from-ny", "posted": "2015-07-29T08:07:00-05:00"}

<table>
	<tr>
		<th>Place From</th>
		<th>20% income</th>
		<th>Middle Class Suburb</th>
		<th>1/3 Median home value in middle class suburb.</th> 
		<th>Upper Class Suburb</th>
		<th>1/3 Median home value in upper class suburb.</th>
	</tr>
	<tr>
		<td><a href="http://statisticalatlas.com/neighborhood/Texas/Houston/Southeast/Household-Income">Southeast Houston, TX</a></td>
		<td>$15,500</td>
		<td><a href="http://quickfacts.census.gov/qfd/states/48/4869596.html">Spring, TX</a></td>
		<td>$36,866</td>
		<td><a href="http://quickfacts.census.gov/qfd/states/48/4872656.html">The Woodlands, TX</a></td>
		<td>$92,667</td>
	</tr>
	<tr>
		<td><a href="http://statisticalatlas.com/county-subdivision/New-York/Bronx-County/Bronx/Household-Income">The Bronx, NY</a></td>
		<td>$12,100</td>
		<td><a href="http://quickfacts.census.gov/qfd/states/36/3642081.html">Levittown, NY</a></td>
		<td>$121,600</td>
		<td><a href="http://quickfacts.census.gov/qfd/states/36/3642081.html">Bayville, NY</a></td>
		<td>$188,633</td>
	</tr>
</table>
