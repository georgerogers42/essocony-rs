{"title": "Income Per Capita 2013", "slug": "income", "posted": "2015-07-11T17:07:00-05:00", "author": "George Rogers"}

<table>
  <tr>
    <th>County</th>
    <th>$Income</th>
    <th>$PPP</th>
    <th>$Income PPP</th>
  </tr>
  <tr>
    <td>Harris County, TX</td>
    <td>$53,141.00</td>
    <td>$1.12</td>
    <td>$59,708.99</td>
  </tr>
  <tr>
    <td>New York County, NY</td>
    <td>$121,632.00</td>
    <td>$0.34</td>
    <td>$59,708.99</td>
  </tr>
  <tr>
    <td>Dallas County, TX</td>
    <td>$48,638.00</td>
    <td>$1.11</td>
    <td>$54,042.22</td>
  </tr>
  <tr>
    <td>Cook County, IL</td>
    <td>$49,661.00</td>
    <td>$0.96</td>
    <td>$47,750.96</td>
  </tr>
  <tr>
    <td>Middlesex County, NJ</td>
    <td>$52,291.00</td>
    <td>$0.78</td>
    <td>$40,852.34</td>
  </tr>
  <tr>
    <td>San Mateo County, CA</td>
    <td>$79,893.00</td>
    <td>$0.44</td>
    <td>$35,040.79</td>
  </tr>
  <tr>
    <td>Queens County, NY</td>
    <td>$44,966.00</td>
    <td>$0.61</td>
    <td>$27,586.50</td>
  </tr>
  <tr>
    <td>Bronx County, NY</td>
    <td>$32,852.00</td>
    <td>$0.72</td>
    <td>$23,805.80</td>
  </tr>
  <tr>
    <td>Kings County, NY</td>
    <td>$42,306.00</td>
    <td>$0.56</td>
    <td>$23,503.33</td>
  </tr>
</table>
