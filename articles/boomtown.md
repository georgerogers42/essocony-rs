{"title": "Boomtown&mdash;Why Houston not Washington DC is the soul of America!", "author": "George Rogers", "slug": "boomtown", "posted": "2015-10-06T15:52:00-05:00"}

What city today represents American values the most?

* Would it be Washington DC, with its halls of power?
* Or the San Francisco Bay Area with its computer industry?

No because no two cities are more Unamerican than these.

* Could it be New York: with its girth, finance and media empires?
* How about Chicago, City of big shoulders capital of the rust belt?

In a past age yes&mdash;But they are dieing cities.

> Then tell me what city represents America's values the most.

* Houston, the oil capital of world represents Americas values the most of any city in the world.

# Why do I claim this

1. [It is a city of industry, with a strong business community](#boomtown-point-1)
2. [It is a city with a strong church](#boomtown-point-2)

<h2 id="boomtown-point-1">1. It is a city of industry, with a strong business community</h2>
>

Houston is a heart of the global oil industry; which has created new jobs by creating new technologies to extract oil from the ground.
For example: fracking, slickwater compounds, and horizontal drilling.

<h2 id="boomtown-point-2">2. It is a city with a strong church</h2>
Zoning is a great enabler of corruption.
Because of the subjective nature of the zoning process many methods of subtle intimidation are enabled.

In every other city can ban businesses from opening due to arbitrary political requirements. But in Houston the City cannot do so; because it lacks that necessary mechanism for corruption.

### So What?
Back in the fall of 2014 there was a fight over the Houston Equal Rights Ordinance or the so called HERO ordinance.
During the fight Mayor Parker was denied the subtle means of control that most other cities have, thereby giving churches victory over city hall.
